const Dev = require('../models/Dev');

module.exports = {
  async store(req, res) {
    const { devId } = req.params;
    const { user: devLoggedId } = req.headers;

    const loggedDev = await Dev.findById(devLoggedId);
    const targetDev = await Dev.findById(devId);

    if (!targetDev) {
      return res.status(400).json({ error: 'Dev not exists' });
    }

    loggedDev.dislikes.push(targetDev._id);

    await loggedDev.save();

    return res.json({
      success: 'All is OK!!!',
      loggedDev
    });
  }
};
